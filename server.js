const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const generator = require('rnamegen');
const uuid = require('uuid/v1');

const { readFile, writeFile, access, F_OK } = require('./fs-promise');
const { LSTM_BOT_VERSION, initializeLSTM, makeMove, startGame } = require('./snakeLearner');
const Charts = require('./charts');

const port = 8987;
const iterationFile =  `${__dirname}/.it`;

const lstm = initializeLSTM(); // Not using this variable, but just in case we need
const charts = new Charts();
const chat = [{name: '@ChatBot', color: '#000000', text: `Welcome to <strong>Snake Learnbot ${LSTM_BOT_VERSION} Chat</strong>! Be respectful and enjoy.`, id: uuid()}];

let gameCount = 0;
let iteration;
let game;
let lastMove;
let userCount = 0;

app.use('/assets', express.static('assets'));

app.get('/', (req, res) => {
	res.sendFile(`${__dirname}/views/liveGame.html`)
});

io.on('connection', function(socket){
	sendWelcome(socket);
	userCount++;

	socket.on('charts', function() {
		socket.emit('charts', charts.getCharts());
	});

	socket.on('chat', message => {
		if(!message.name) {
			message.name = generator(1, 5, 9)[0];
			socket.emit('name', message.name);
		}
		message.text = message.text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		message.id = uuid();
		chat.push(message);
		io.emit('chat', message);
	});

	socket.on('disconnect', () => {
		userCount--;
	});
});

http.listen(port, () => console.log(`Example app listening on port ${port}!`));

start();

function sendWelcome(socket) {
	socket.emit('welcome', {
		...{LSTM_BOT_VERSION, iteration, gameCount, lastMove: lastMove ? lastMove.turn || 0 : 0}, 
		...(game ? {snapshot: game.snapshot()} : {}),
	}, chat);
}

function broadcastMove() {
	io.emit('move', game.snapshot(), userCount);
	newIteration();
}

function wait(n = 500) {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve();
		}, n);
	});
}

function start() {
	updateIterationFile()
	.then(() => {
		newIteration();
	})
}

function updateIterationFile() {
	return ensureIterationFile()
	.then(() => {
		return readFile(iterationFile);
	})
	.then(data => {
		iteration = parseInt('' + data);
		return saveNextIteration();
	})
	.catch(err => {
		console.error(err);
		process.exit(1);
	});
}

function ensureIterationFile() {
	return new Promise((resolve, reject) => {
		access(iterationFile, F_OK)
		.then(() => {
			resolve();
		})
		.catch(() => {
			writeFile(iterationFile, '0')
			.then(() => {
				resolve();
			})
			.catch(err => {
				reject(err);
			});
		});
	});
} 

function saveNextIteration() {
	return writeFile(iterationFile, '' + (iteration + 1));
}

function newGame() {
	if(game) charts.addGame(game, lastMove);
	game = startGame();
	io.emit('newGame', gameCount++);
}

function newIteration() {
	if(!game || !game.isStillRunning()) {
		wait(1000)
		.then(() => {
			newGame();
			broadcastMove();
		});
	} else {
		wait(200)
		.then(() => {
			lastMove = makeMove();
			broadcastMove();
		});
	}
}