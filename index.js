const { readFile } = require('fs-extra');
const { Network, Layer } = require('synaptic');
const prompts = require('prompts');
const ProgressBar = require('progress');

function readFilePromise(file) {
	return new Promise((resolve, reject) => {
		readFile(file, (err, data) => {
			if(err) return reject(err);
			resolve(String(data));
		});
	});
}

if(process.argv.length > 2 && ['-h', '--help'].includes(process.argv[2])) {
	console.log(`${process.argv[0]} ${process.argv[1]} <nTexts | 10> <nWords | 5> <nIters | 500000>`);
	process.exit();
}
const nTexts = process.argv.length > 2 ? parseInt(process.argv[2]) : 10;
const nWords = process.argv.length > 3 ? parseInt(process.argv[3]) : 5;
const nIters = process.argv.length > 4 ? parseInt(process.argv[4]) : 500000;
let wordIds = {}, wordList;
let count = 0;

console.log('Please, wait until we learn');

Promise.all(new Array(nTexts).fill().map((_, i) => readFilePromise(`text${i}.txt`)))
.then(texts => {
	texts.forEach(t => {
		t.split(' ').forEach(word => {
			if (!Object.hasOwnProperty.call(wordIds, word)) {
				wordIds[word] = count++;
			}
		});
	});

	const keys = Object.keys(wordIds);
	keys.forEach(key => {
		wordIds[key] /= (keys.length-1);
	});
	wordList = Object.entries(wordIds);
	neuralExperiment(texts);
})
.catch(error => {
	console.error(error);
});

// function randomWord(code = false) {
// 	return wordList[parseInt(Math.random()*wordList.length)][code ? 1 : 0];
// }

// function randomSentence(code = false) {
// 	const result = new Array(nWords).fill().map(() => randomWord(code));
// 	return code ? result : result.join(' ');
// }

function sentenceToCodeArray(sentence) {
	return sentence.split(' ').map(word => wordIds[word]);
}

function codeArrayToSentence(codeArray) {
	return codeArray.map(wordCode => wordList.find(([_, code]) => code === wordCode)[0]).join(' ');
}

function neuralExperiment(texts) {

	const textsArrays = texts.map(text => sentenceToCodeArray(text));

	const learningRate = .3;

	const inputLayer = new Layer(nWords);
	const hiddenLayer1 = new Layer(parseInt(nWords*1.5));
	const hiddenLayer2 = new Layer(parseInt(nWords*0.5));
	const outputLayer = new Layer(1);

	const myNetwork = new Network({
		input: inputLayer,
		hidden: [hiddenLayer1, hiddenLayer2],
		output: outputLayer
	});

	inputLayer.project(hiddenLayer1);
	hiddenLayer1.project(hiddenLayer2);
	hiddenLayer2.project(outputLayer);

	const bar = new ProgressBar(':bar', { total: nIters });
	for(let i = 0; i < nIters; i++) {
		learnIteration();
		bar.tick();
	}

	while(!bar.complete) {
		bar.tick();
	}

	console.log('We are set! ready to go');
	askSentence();

	function learnIteration(propagate = true) {
		const { text, sentence } = randomSentence();
		// console.log(`Text ${text}\nSentece ${sentence.join(' ')} (${codeArrayToSentence(sentence)})`);
		const textCode = text / (texts.length-1);
		
		const [result] = myNetwork.activate(sentence);
		if(propagate) myNetwork.propagate(learningRate, [textCode]);
		return Math.round(result*(texts.length-1)) == text;
	}

	function classify(sentence) {
		console.log(sentence);
		return myNetwork.activate(sentence)[0];
	}
	
	function askSentence() {
		prompts({
			type: 'text',
			name: 'sentence',
			message: 'Type your sentence',
			validate: sentence => sentence === 'exit' || sentence === 'test' || sentence.split(' ').length === nWords ? true : `Please, use ${nWords} words sentences`,
		})
		.then(({sentence}) => {
			if(sentence === 'exit') {
				console.log('bye!');
				process.exit();
			}
			if(sentence === 'test') {
				let success = 0;
				const TOTAL = 1000;
				for(let i = 0; i < TOTAL; i++) {
					if(learnIteration(false)) success++;
				}
				console.log(`Percentage success: ${(success/TOTAL*100).toFixed(2)}%`);
			} else {
				const result = classify(sentenceToCodeArray(sentence));
				console.log(result);
				const idResult = Math.round(result*(texts.length-1));
				console.log(`Result is ${result}, which corresponds with text ${idResult}.
STATUS: ${texts[idResult].includes(sentence) ? 'SUCCESS' : 'FAILURE'}
TEXTS BY STATUS: [${texts.map((text, i) => `${i}: ${text.includes(sentence)}`).join(', ')}]`);
			}
			askSentence();
		});
	}

	function randomSentence() {
		const text = Math.min(parseInt(Math.random()*texts.length), texts.length-1);
		const chosenText = textsArrays[text];
		const chosenSentence = parseInt(Math.random()*(chosenText.length - nWords));

		return {
			text,
			sentence: new Array(nWords).fill().map((_, i) => chosenText[chosenSentence + i]),
		};
	}

}

