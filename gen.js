const { writeFile } = require('fs-extra');

let dicc = 'abcdefghijklmnopqrstuvwxyz'.split('');

if(process.argv.length > 2 && ['-h', '--help'].includes(process.argv[2])) {
	console.log(`${process.argv[0]} ${process.argv[1]} <nTexts | 10> <nWords | 5000> <nLetters | 10>`);
	process.exit();
}
const nTexts = process.argv.length > 2 ? parseInt(process.argv[2]) : 10;
const nWords = process.argv.length > 3 ? parseInt(process.argv[3]) : 5000;
const nLetters = process.argv.length > 4 ? parseInt(process.argv[4]) : 10;

function newWord() {
	return new Array(nLetters).fill().map(() => dicc[parseInt(Math.random()*dicc.length)]).join('');
}

function newText() {
	return new Array(nWords).fill().map(() => newWord()).join(' ');
}

function writeFilePromise(file, text) {
	return new Promise((resolve, reject) => {
		writeFile(file, text, err => {
			if(err) return reject(err);
			resolve();
		});
	});
}

const texts = [];

for(let i = 0; i < nTexts; i++) {
	texts.push(newText());
}

Promise.all(texts.map((text, i) => writeFilePromise(`text${i}.txt`, text)))
.then(() => {
	console.log('Finished!');
})
.catch(err => {
	console.error(err);
});