const config = {

	GAME: {
		ROWS: 10,
		COLUMNS: 10,
	},

	LSTM: {
		HIDDEN: [100],
		OUTPUT: 1,
		WALK_LEARNING_RATE: 0.01,
		BORENESS_LEARNING_RATE: 0.001,
		DYING_LEARNING_RATE: 0.1,
		EATING_LEARNING_RATE: 0.9,
		LEARNING_RATE: 0.3,
		BORENESS_THRESHOLD: 20,
		BORENESS_INCREMENT: 0.01,
	},

	EXPERIMENT: {
		MAX_EXPERIMENTS: 1000000,
		ITERATIONS: 200,
		BORENESS_THRESHOLD: 20,
		MAX_GAME_LENGTH: 999999,
		PATH: './experiments',
		TEMPLATES: {
			HTML_TEMPLATE: './gameTemplate.html',
			CSS_TEMPLATE: './assets/game.css',
			JS_TEMPLATE: './assets/game.js',
			CSS_TEMPLATE_VAR: '{{gameStyle}}',
			JS_TEMPLATE_VAR: '{{gameScript}}',
		},
		OUTPUT: {
			RESULT_REPLAY_NAME_SCRIPT: 'replay.js',
			RESULT_REPLAY_NAME_JSON: 'replay.json',
			RESULT_LSTM_NAME_JSON: 'synaptic-lstm.json',
			SAVE_LSTM: false,
			RESULT_HTML: 'replay.html',
			LOG_FILE: 'log.txt',
		},
	},


};

config.LSTM.INPUT = config.GAME.ROWS*config.GAME.COLUMNS*2+2;

module.exports = config;