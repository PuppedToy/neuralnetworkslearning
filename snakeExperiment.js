const ProgressBar = require('progress');

const { readFile, writeFile, readdir, mkdir, getLastCommit } = require('./fs-promise');
const config = require('./snakeConfig');
const { LSTM_BOT_VERSION, initializeLSTM, playGame } = require('./snakeLearner');

const SNAKE_GAME_VERSION = require('./snake').version();

let template;
let logCount = 0;
let log = '';

/***** EXPERIMENT ******/
/***********************/
/***********************/

startExperiment();

/***********************/
/***********************/
/***********************/


/****** FUNCTIONS ******/
/***********************/

function loadTemplates() {
	Promise.all([
		readFile(config.EXPERIMENT.TEMPLATES.HTML_TEMPLATE),
		readFile(config.EXPERIMENT.TEMPLATES.CSS_TEMPLATE),
		readFile(config.EXPERIMENT.TEMPLATES.JS_TEMPLATE),
	])
	.then(templates => {
		const [html, css, js] = templates.map(t => String(t));
		return new Promise(resolve => {
			template = html.replace(config.EXPERIMENT.TEMPLATES.CSS_TEMPLATE_VAR, css).replace(config.EXPERIMENT.TEMPLATES.JS_TEMPLATE_VAR, js);
			resolve();
		});
	})
	.catch(err => {
		console.error(err);
		process.exit();
	})
}

function withZeros(n, max) {
	return new Array(String(max-1).length - String(n).length).fill('0').join('') + n;
}

function initExperimentDirectory() {
	let experimentId = null;
	return new Promise((resolve) => {
		mkdir(config.EXPERIMENT.PATH, { recursive: true })
		.then(() => {
			return readdir(config.EXPERIMENT.PATH);
		})
		.then(directories =>  {
			const latestExperiment = Math.max(
				-1,
				...directories
					.map(directory => parseInt(directory))
					.filter(directoryId => !isNaN(directoryId))
			);

			experimentId = latestExperiment + 1;
			if(experimentId >= config.EXPERIMENT.MAX_EXPERIMENTS) throw new Error(`Max experiments reached`);
			experimentId = withZeros(experimentId, config.EXPERIMENT.MAX_EXPERIMENTS);

			return Promise.all([
				getLastCommit(),
				mkdir(`${config.EXPERIMENT.PATH}/${experimentId}`),
				loadTemplates(),
			]);
		})
		.then(([lastCommit]) => {
			const experimentDescription = {
				SNAKE_GAME_VERSION,
				LSTM_BOT_VERSION,
				config,
				lastCommit,
			};

			return writeFile(`${config.EXPERIMENT.PATH}/${experimentId}/experimentDescription.json`, 
				JSON.stringify(experimentDescription));
		})
		.then(() => {
			resolve(experimentId);
		})
		.catch(error => {
			console.error(error);
			process.exit();
		});
	});
}

function saveExperiment(lstm, game, experimentId, iteration) {
	const baseDir = `${config.EXPERIMENT.PATH}/${experimentId}/${withZeros(iteration, config.EXPERIMENT.ITERATIONS)}`;

	return new Promise((resolve, reject) => {
		Promise.resolve()
		.then(() => {
			return mkdir(baseDir);
		})
		.then(() => {
			return Promise.all([
				writeFile(`${baseDir}/${config.EXPERIMENT.OUTPUT.RESULT_REPLAY_NAME_SCRIPT}`, `const game = JSON.parse('${JSON.stringify(game.history)}');`),
				writeFile(`${baseDir}/${config.EXPERIMENT.OUTPUT.RESULT_REPLAY_NAME_JSON}`, JSON.stringify(game.history)),
				writeFile(`${baseDir}/${config.EXPERIMENT.OUTPUT.RESULT_HTML}`, template),
				writeFile(`${baseDir}/${config.EXPERIMENT.OUTPUT.LOG_FILE}`, log),
				config.EXPERIMENT.OUTPUT.SAVE_LSTM ?
					writeFile(`${baseDir}/${config.EXPERIMENT.OUTPUT.RESULT_LSTM_NAME_JSON}`, JSON.stringify(lstm.toJSON()))
					: Promise.resolve(),
			]);
		})
		.then(() => {
			resolve();
		})
		.catch(error => {

		});
	});
}

function startExperiment() {
	console.log('Setting experiment up...');
	initExperimentDirectory()
	.then(experimentId => {
		return experiment(experimentId);
	})
	.then(() => {
		console.log('Ended experiment!');
	})
	.catch(error => {
		console.error(error);
	});
}

async function experiment(experimentId) {

	const lstm = initializeLSTM();
	const bar = new ProgressBar(':bar', { total: config.EXPERIMENT.ITERATIONS });
	const iterationCallback = iterationData => {
		log += `--- Turn ${iterationData.turn} ---\n`;
		log += `Decision: ${iterationData.decision}\n`;
		log += `Action: ${iterationData.action}\n`;
		log += `Results: ${JSON.stringify(iterationData.result)}\n`;
		log += `Boreness rate: ${iterationData.borenessRate}\n`;
		log += `Feedback provided: [${iterationData.feedbackType}, ${iterationData.learningType}]\n`;
		log += `---------------------------\n\n`;
		const breakCondition = iterationData.turn >= config.EXPERIMENT.MAX_GAME_LENGTH;
		if (breakCondition) {
			console.log(`WARNING: ${config.EXPERIMENT.MAX_GAME_LENGTH} iters`);
		}
		return breakCondition;
	};

	for(let iteration = 0; iteration < config.EXPERIMENT.ITERATIONS; iteration++) {
		log = '';
		const game = playGame(lstm, iterationCallback);
		await saveExperiment(lstm, game, experimentId, iteration);
		bar.tick();
	}

}
