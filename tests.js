const { Architect } = require('synaptic');
const { sample } = require('./utils');

const perceptron = new Architect.LSTM(4, 4, 1);

const sets = buildSets(8);

sets.forEach(set => {
	console.log(set);
	console.log(perceptron.activate(set));
});

for(let i = 0; i < 1000; i++) {
	perceptron.activate(sample(sets));
}

console.log('------');

sets.forEach(set => {
	perceptron.activate(sets[0]);
	perceptron.propagate(0.3, [0.7]);
});

sets.forEach(set => {
	console.log(set);
	console.log(perceptron.activate(set));
});

console.log('------');

sets.forEach(set => {
	console.log(set);
	console.log(perceptron.activate(set));
});

function buildSets(n) {
	return new Array(n).fill().map(() => buildSet());
}

function buildSet() {
	return new Array(4).fill().map(() => Math.random() < 0.5 ? 0 : 1);
}
