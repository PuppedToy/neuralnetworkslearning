const Game = require('./snake');
const { sample } = require('./utils');
const config = require('./snakeConfig');
const { writeFile, readFile } = require('./fs-promise');

const LSTM_BOT_VERSION = '3.1';

const DIRECTION_LIST = [
	['RIGHT', 'R'],
	['LEFT', 'L'],
	['UP', 'U'],
	['DOWN', 'D'],
];

const OPPOSITE_DIR = {
	'RIGHT': 'LEFT',
	'LEFT': 'RIGHT',
	'UP': 'DOWN',
	'DOWN': 'UP',
};

const DIRECTION_TRANSLATION_TABLE = {
	RIGHT: 'R',
	LEFT: 'L',
	UP: 'U',
	DOWN: 'D',
	null: ' '
};

const KNOWLEDGE_TEMPLATE = {
	R: {expectedReward: 0, expectedValue: 0, explorationRate: 0, knownTransition: null},
	L: {expectedReward: 0, expectedValue: 0, explorationRate: 0, knownTransition: null},
	U: {expectedReward: 0, expectedValue: 0, explorationRate: 0, knownTransition: null},
	D: {expectedReward: 0, expectedValue: 0, explorationRate: 0, knownTransition: null},
};

let lstm, game;

function getState(lastMove, snapshot) {
	const { board } = snapshot;

	return `${DIRECTION_TRANSLATION_TABLE[lastMove]}${board.flat(2).join('')}`
}

let stateAnalysisQueue = [];
// Some thoughts: I need to use webworkers to do this. Or divide better the calculations
const ANALYSIS_INTERVAL = 200, DISCOUNT_RATE = 0.9, ANALYSIS_MAX_ITERATIONS = 10;

function analyzeKnowledge() {
	// console.log(`------------- CURRENT KNOWLEDGE -------------\n\n${JSON.stringify(knowledge)}\n----------- END CURRENT KNOWLEDGE -----------\n\n\n`);
	saveKnowledge();
	stateAnalysisQueue = Object.keys(knowledge);
	analyzeNextState();
}

function analyzeNextState() {
	if(stateAnalysisQueue.length > 0) {
		const state = knowledge[stateAnalysisQueue.shift()];
		Object.values(state).forEach(stateAction => {
			if(stateAction.expectedReward) {
				// This is very wrong right now. Reward should never be more than 1, but it's being because we are adding it up. Check how to do it without sums.
				// Also knownTransition should be null if reward is 1. Please, check it out. This is already sorted out
				stateAction.expectedValue = stateAction.expectedReward;
				stateAction.explorationRate = 0; // TODO understand this
			}
			else if(stateAction.knownTransition) {
				const {expectedValue, explorationRate} = calculateActionValue(stateAction.knownTransition);
				stateAction.expectedValue = expectedValue;
				stateAction.explorationRate = explorationRate;
			}
		});
	}
	setTimeout(() => {
		if(stateAnalysisQueue.length <= 0) analyzeKnowledge();
		else analyzeNextState();
	}, ANALYSIS_INTERVAL);
}

// TODO Maybe we should trunk already visited states (like in an array). We use a global array where we put all the states visited and if I 
// have already evaluated one this round, we do not evaluate again
// In fact, we might trunk even more loopy states. But we shall see in the future, when snake plays better
function calculateActionValue(stateName, iteration = 1) {
	// console.log(`${new Array(iteration).fill().map(() => '--').join('')} Analizing "${stateName}"... `);
	if(!stateName || !Object.hasOwnProperty.call(knowledge, stateName)) {
		// If there is no info about this state, we increment explore chances
		// console.log(`> ${new Array(iteration).fill().map(() => '--').join('')} --> 0 (TS)`);
		return {
			expectedValue: 0,
			explorationRate: 0.1,
		};
	}
	const state = knowledge[stateName];
	const discount = DISCOUNT_RATE**iteration;
	const stateActions = Object.values(state);
	let reward = 0, negativeCount = 0;

	// Explore either two options:
	// 		1. A reward is found in any action
	// 		2. 3 of the possible actions return -1. This means there is no way out, so it is necessary to avoid. 
	// 			Point 2 could be precalculated.
	// If this is the case, stop exploring this branch. Else, go find the reward somewhere deeper
	stateActions.forEach(stateAction => {
		if(stateAction.expectedReward > 0) {
			reward = 1;
		}
		else if(stateAction.expectedReward < 0) {
			negativeCount++;
		}
	});
	if(!reward && negativeCount >= 3) {
		reward = -1;
	}
	if(reward !== 0) {
		// console.log(`${new Array(iteration).fill().map(() => '--').join('')} TERMINAL STATE: "${stateName}" is ending value: ${discount}*${reward} = ${discount*reward} ------`);
		return {
			expectedValue: discount*reward,
			explorationRate: 0,
		};
	}

	// If we reach here, time to explore deeper. Go trough every possible action and find the reward
	let totalExpectedValue = 0, totalExplorationRate = 0;
	if(iteration <= ANALYSIS_MAX_ITERATIONS) {
		stateActions.forEach(stateAction => {
			// We only explore rewardless options because they are non terminal.
			if(stateAction.expectedReward === 0) {
				const {expectedValue, explorationRate} = calculateActionValue(stateAction.knownTransition, iteration + 1);
				// If we found a dead end, we should propagate avoiding it
				// Else we should propagate reward
				if(expectedValue >= 0) totalExpectedValue = Math.max(expectedValue, totalExpectedValue);
				else totalExpectedValue = Math.min(expectedValue, totalExpectedValue);

				// TODO care with exploration, while we track the 4 actions, there will always be one unknown. If exploration becomes a thing, we should take care of this issue
				totalExplorationRate += explorationRate;
				// console.log(`> ${new Array(iteration).fill().map(() => '--').join('')} --> ${expectedValue} | ${result.expectedValue} (CH)`);
			}
		});
	}

	// console.log(`${new Array(iteration).fill().map(() => '--').join('')} NON TERMINAL STATE: "${stateName}" is ending with value: ${JSON.stringify({totalExpectedValue, totalExplorationRate})} ------`);
	return {
		expectedValue: totalExpectedValue,
		explorationRate: totalExplorationRate,
	};
}

let knowledge;

function initializeLSTM(overrideConfig = {}) {
	readKnowledge()
	.then(() => {
		analyzeKnowledge();
	});
}

function saveKnowledge() {
	const data = JSON.stringify(knowledge);
	writeFile('knowledge.json', data)
	.catch(error => {
		console.error(error);
	});
}

function readKnowledge() {
	return new Promise(resolve => {
		readFile('knowledge.json')
		.then(data => {
			knowledge = JSON.parse('' + data);
		})
		.catch(() => {
			knowledge = {};
			console.log('knowledge file not found.');
		})
		.finally(() => {
			resolve();
		});
	});
}

let gameMemory = {};

function startGame() {
	gameMemory = {
		gameRunning: true,
		turn: 0,
		borenessRate: 0,
		lastMoves: [],
	};
	game = new Game(config.GAME.ROWS, config.GAME.COLUMNS);
	return game;
};

function playGame(iterationCallback) {

	startGame();
	
	while(gameMemory.gameRunning) {
		const {turn} = gameMemory;
		const move = makeMove();
		if(iterationCallback) {
			const breakCondition = iterationCallback({...move, turn});
			if(breakCondition) break;
		}
	}
	return game;

}

function calculateReward({growth, gameStillRunning}) {
	if(!gameStillRunning) return -1;
	else if(growth) return 1;
	else return 0;
}

function createEmptyState() {
	return Object.entries(KNOWLEDGE_TEMPLATE).reduce((object, [action, template]) => ({
		...object,
		[action]: {...template},
	}), {});
}

function getActionValue(action, state) {
	if(!knowledge[state]) {
		knowledge[state] = createEmptyState();
	}
	// console.log(action);
	return knowledge[state][action].expectedValue;
}

function learn(action, state, newState, reward) {
	if(!Object.hasOwnProperty.call(knowledge, state)) {
		// console.log(`state "${state}" does not exist!`);
		knowledge[state] = createEmptyState();
	}
	knowledge[state][DIRECTION_TRANSLATION_TABLE[action]].expectedReward = reward;
	if(reward == 0) knowledge[state][DIRECTION_TRANSLATION_TABLE[action]].knownTransition = newState;
	else knowledge[state][DIRECTION_TRANSLATION_TABLE[action]].knownTransition = null;
}

function getActionValues(state, lastMove) {
	return DIRECTION_LIST.filter(([actionName]) => !lastMove || actionName !== OPPOSITE_DIR[lastMove]).reduce((object, [actionName, action]) => ({
		...object,
		[actionName]: getActionValue(action, state),
	}), {});
}

function getChanceExplore(state) {
	if(Object.hasOwnProperty.call(knowledge, state)) {
		return Object.values(knowledge[state]).reduce((total, {explorationRate}) => total + explorationRate, 0);
	} else {
		return 1;
	}
}

function chooseAction(actionValues, isExploring) {
	if(isExploring) {
		// TODO remove walls from exploring and do explore more often
		// return sample(Object.keys(actionValues).filter(key => actionValues[key] >= 0));
		return sample(Object.keys(actionValues));
	} else {
		const bestActions = Object.entries(actionValues).sort((value1, value2) => value2[1] - value1[1]);
		const bestValue = bestActions[0][1];
		const filteredBestActions = bestActions.filter(([_, value]) => value === bestValue);
		return sample(filteredBestActions)[0];
	}
}

function makeMove() {
	// let log = `------------- START MOVE ${gameMemory.turn} -------------\n\n`;
	if(!gameMemory.gameRunning) throw new Error('No game is running!');
	const snapshot = game.snapshot();
	const lastMove = game.getLastMove();
	// log += `---- Previous Move: ${lastMove}\n`;
	const state = getState(lastMove, snapshot);
	// log += `---- State: ${state}\n`;

	const actionValues = getActionValues(state, lastMove);
	// console.log(`<${state}>`);
	// console.log(JSON.stringify(actionValues));
	// log += `---- Action Values: ${JSON.stringify(actionValues)}\n`;
	// const chanceExplore = getChanceExplore(state);
	const chanceExplore = 0.01;
	// log += `---- Chance Explore: ${chanceExplore}\n`;
	const isExploring = Math.random() < chanceExplore;
	// log += `---- isExploring: ${isExploring}\n`;
	const chosenAction = chooseAction(actionValues, isExploring);
	// log += `---- chosenAction: ${chosenAction}\n`;

	const moveResult = game.moveSnake(chosenAction);
	// log += `---- Moved Snake! Move Result: ${JSON.stringify(moveResult)}\n`;
	const {growth, gameStillRunning} = moveResult;

	gameMemory.gameRunning = gameStillRunning;
	if(growth) gameMemory.borenessRate = 0;
	else gameMemory.borenessRate++;

	const reward = calculateReward(moveResult);
	// log += `---- Reward: ${reward}\n`;
	const newState = getState(chosenAction, game.snapshot());
	// log += `---- New State: ${newState}\n`;
	learn(chosenAction, state, newState, reward);
	
	// log += `-------------- END MOVE ${gameMemory.turn} --------------\n\n\n`;
	// console.log(log);
	gameMemory.turn++;
	return {snapshot, actionValues, chosenAction, growth, isExploring, gameStillRunning, ...gameMemory};
}

module.exports = {
	LSTM_BOT_VERSION,
	initializeLSTM,
	playGame,
	makeMove,
	startGame,
};