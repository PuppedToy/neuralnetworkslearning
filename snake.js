const { sample } = require('./utils');

const GAME_VERSION = '1.0.5';

const GAME_STATES = {
	RUNNING: 0,
	ERROR: 1,
	LOSE: 2,
};

const GAME_ITEMS = {
	FOOD: 0,
	SNAKE_PART: 1,
	SNAKE_HEAD: 2,
};

const DIRECTIONS = {
	LEFT: [-1, 0],
	RIGHT: [1, 0],
	UP: [0, -1],
	DOWN: [0, 1],
};

const OPPOSITE_DIRECTIONS = {
	LEFT: 'RIGHT',
	RIGHT: 'LEFT',
	UP: 'DOWN',
	DOWN: 'UP',
};

class Game {

	constructor(rows = 10, columns = 10) {
		this.rows = 10;
		this.columns = 10;
		this.board = new Array(this.rows).fill().map((_, row) => 
			new Array(this.columns).fill().map((_, column) => new Cell(row, column))
		);
		this.snake = new Snake(this.getRandomCell());
		this.getRandomEmptyCell().setItem('FOOD');
		this.changeGameState('RUNNING');
		this.lastMove = null;
		this.previousMove = null;
		const initialSnapshot = this.snapshot();
		this.history = [initialSnapshot];
		this.replay = {
			initialSnapshot,
			moves: [],
		}
	}

	moveSnake(direction) {
		let growth = false;
		if(this.isStillRunning()) {
			this.checkDirection(direction);

			if(this.lastMove === OPPOSITE_DIRECTIONS[direction]) {
				direction = OPPOSITE_DIRECTIONS[direction];
			}
			const newCell = this.getAdjacentCell(this.snake.head.cell, direction);
			if(!newCell || newCell.hasItem('SNAKE_PART') && newCell !== this.snake.head.next.cell) {
				this.lose();
			} else {
				growth = newCell.hasItem('FOOD');
				newCell.setItem();
				this.snake.move(newCell, growth);
				if(growth) {
					this.getRandomEmptyCell().setItem('FOOD');
				}
				this.history.push(this.snapshot());
				this.replay.moves.push(direction.charAt(0));
			}
			this.previousMove = this.lastMove;
			this.lastMove = direction;
		}
		return {growth, gameStillRunning: this.isStillRunning()};
	}

	lose() {
		this.changeGameState('LOSE');
	}

	isStillRunning() {
		return this.state === GAME_STATES['RUNNING'];
	}

	changeGameState(newState) {
		if(!Object.hasOwnProperty.call(GAME_STATES, newState)) throw new Error(
			`Game state "${newState}" does not exist. Existing ones: {${Object.keys(GAME_STATES).join(", ")}}`);

		this.state = GAME_STATES[newState];
	}

	getCell(row, column) {
		if(row < 0 || row >= this.rows || column < 0 || column >= this.columns) return null;
		return this.board[row][column];
	}

	getSnakeSize() {
		return this.snake.size;
	}

	getLastMove() {
		return this.lastMove;
	}

	getPreviousMove() {
		return this.previousMove;
	}

	getAdjacentCell(cell, direction) {
		const [addX, addY] = DIRECTIONS[direction];
		const result = this.getCell(cell.row + addY, cell.column + addX);
		return result;
	}

	getRandomEmptyCell() {
		const availableCells = this.board.flat().filter(cell => cell.isEmpty());
		if(availableCells.length <= 0) throw new Error(`There are no available cells. Board = ${JSON.stringify(this.board)}`);
		return sample(availableCells);
	}

	getRandomCell() {
		return sample(sample(this.board));
	}

	checkDirection(direction) {
		if(!Object.hasOwnProperty.call(DIRECTIONS, direction)) throw new Error(`
			Direction "${direction}" does not exist. Existing ones: {${Object.keys(DIRECTIONS).join(", ")}}`);
	}

	snapshot() {
		return new SnapShot(this);
	}

	static version() {
		return GAME_VERSION;
	}

}

class Cell {

	constructor(row, column) {
		this.row = row;
		this.column = column;
		this.item = null;
	}

	isEmpty() {
		return !this.item;
	}

	setItem(item) {
		if(!item) { 
			this.item = null;
			return;
		}
		this.checkItem(item);
		this.item = GAME_ITEMS[item];
	}

	hasItem(item) {
		if(!item) return this.item !== null;
		this.checkItem(item);
		return this.item === GAME_ITEMS[item];
	}

	areAdjacent(cell) {
		const areVerticallyAdjacent = Math.abs(this.row - cell.row) === 1;
		const areHorizontallyAdjacent = Math.abs(this.column - cell.column) === 1;
		return  areVerticallyAdjacent && !areHorizontallyAdjacent || !areVerticallyAdjacent && areHorizontallyAdjacent;
	}

	checkItem(item) {
		if(!Object.hasOwnProperty.call(GAME_ITEMS, item)) throw new Error(`
			Item "${item}" does not exist. Existing ones: {${Object.keys(GAME_ITEMS).join(", ")}}`);
	}

	toString() {
		return `(${this.row}, ${this.column})`;
	}

}

class Snake {

	constructor(startingCell) {
		this.size = 1;
		this.head = new SnakePart(startingCell);
		this.tail = this.head;
	}

	move(cell, hasGrowth) {
		if(!this.head.cell.areAdjacent(cell)) throw new Error(`Cell ${cell.toString()} is not adjacent to head ${this.head.cell.toString()}`);
		const previousHead = this.head;
		this.head = new SnakePart(cell, previousHead);
		if(!hasGrowth) {
			this.tail.cell.setItem();
			this.tail = this.tail.previous;
		} else {
			this.size++;
		}
	}

}

class SnakePart {

	constructor(cell, next = null) {
		this.cell = cell;
		this.setNext(next);
		this.cell.setItem('SNAKE_HEAD');
	}

	checkSnakePart(snakePart) {
		if(snakePart !== null && !(snakePart instanceof SnakePart)) throw new Error(`snakePart is supposed to be a SnakePart. Found ${snakePart}`);
	}

	setNext(snakePart) {
		this.checkSnakePart(snakePart);

		this.next = snakePart || null;
		// Careful, maybe we need to recursively disconnect everyone
		if(this.next) {
			this.next.previous = this;
			this.next.cell.setItem('SNAKE_PART');
		}
	}

}

const SNAPSHOT_ITEM = {
	EMPTY: ' ',
	FOOD: 'X',
	SNAKE_PART: 'O',
	SNAKE_HEAD: 'H',
};

class SnapShot {

	constructor(game) {
		const { board, rows, columns } = game;
		this.rows = rows;
		this.columns = columns;
		this.board = board.map(row => row.map(cell => {
			switch(cell.item) {
				case GAME_ITEMS['FOOD']:
					return SNAPSHOT_ITEM['FOOD'];
				case GAME_ITEMS['SNAKE_HEAD']:
					return SNAPSHOT_ITEM['SNAKE_HEAD']
				case GAME_ITEMS['SNAKE_PART']:
					return SNAPSHOT_ITEM['SNAKE_PART'];
				default:
					return SNAPSHOT_ITEM['EMPTY'];
			}
		}));
	}

	toString() {
		const line = new Array(this.columns+1).fill().map(() => ' ').join('–');
		return `${line}\n${this.board.map(row => `|${row.join(' ')}|`).join('\n')}\n${line}`;
	}

}

module.exports = Game;