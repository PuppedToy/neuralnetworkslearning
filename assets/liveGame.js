let current = 1;
let nMoves;
let $currentChart = null;
let unreadMessages = 0;
let lastMessageId;

const CELL_CLASS = {
	' ': 'cell',
	'O': 'cell snake',
	'H': 'cell snake head',
	'X': 'cell food',
};

function initializeUnreadMessages(chat) {
	lastMessageId = window.localStorage.getItem('snakeLastMessage');
	unreadMessages = 0;
	chat.forEach(({id}) => {
		unreadMessages++;
		if(id === lastMessageId) unreadMessages = 0;
	});
	updateUnreadMessages();
}

$(document).ready(() => {

	rememberName();
	const socket = io();

	socket.on('welcome', ({LSTM_BOT_VERSION, iteration, gameCount, lastMove, snapshot}, chat) => {
		setBotVersion(LSTM_BOT_VERSION);
		setGameId(gameCount);
		setIteration(iteration);
		nMoves = lastMove;
		setNMoves();
		if(snapshot) paintSnapshot(snapshot);
		initializeUnreadMessages(chat);
		paintChat(chat);
	});

	socket.on('newGame', gameId => {
		setGameId(gameId);
		nMoves = 0;
		setNMoves();
	});

	socket.on('move', (snapshot, userCount) => {
		paintSnapshot(snapshot);
		setNMoves();
		nMoves++;
		$(".userCount").html(`${userCount} user${userCount == 1 ? '' : 's'} connected`);
	});

	socket.on('charts', charts => {
		showCharts(charts);
	});

	socket.on('chat', message => {
		chatAddMessage(message);
		unreadMessages++;
		updateUnreadMessages();
	});

	socket.on('name', name => {
		if(!getName()) window.localStorage.setItem('snakeColorName', name);
		saveName(name);
	});

	$("#chartsButton").on('click', () => {
		socket.emit('charts');
	});

	$("#popupCross").on('click', () => {
		hideCharts();
	});

	const showChat = () => {
		$("#chatPopup").css('display', '');
		$("#chatPopup").removeClass('hide');
		$("#chatPopup").removeClass('show');
		$("#chatPopup").addClass('show');
		$("#chat").scrollTop(99999999999);
		setTimeout(() => {
			$("#chatPopup").removeClass('show');
			$("#chatPopup").show();
		}, 500);
	};
	$("#chatUnreadMessages").on('click', showChat);
	$("#chatButton").on('click', showChat);

	$("#minimizeChatButton").on('click', () => {
		unreadMessages = 0;
		if(lastMessageId) window.localStorage.setItem('snakeLastMessage', lastMessageId);
		updateUnreadMessages();
		$("#chatPopup").removeClass('show');
		$("#chatPopup").removeClass('hide');
		$("#chatPopup").addClass('hide');
		setTimeout(() => {
			$("#chatPopup").removeClass('hide');
			$("#chatPopup").hide();
		}, 500);
	});

	const sendMessage = () => {
		const text = $("#messageInput").val();
		const name = $("#nameInput").val().replace(/[^-._a-zA-Z0-9]/g, '');
		if(name) saveName(name);
		const color = getColor();
		if(text) socket.emit('chat', {text, name, color});
		$("#messageInput").val("");
	}

	$("#sendMessageButton").on('click', sendMessage);
	$("#messageInput").on('keypress', ({which}) => {
		if(which == 13) sendMessage();
	});

});

function chatAddMessage({name, color, text, id}) {
	$("#chat").append(`<div class='entry'><span style='color: ${color};'>${name}: </span>${text}</div>`);
	$("#chat").scrollTop(99999999999);
	lastMessageId = id;
}

function saveName(name) {
	$("#nameInput").val(name);
	window.localStorage.setItem('snakeName', name);
}

function getName() {
	return window.localStorage.getItem('snakeName');
}

function rememberName() {
	const name = getName();
	if(name) $("#nameInput").val(name);
}

function getColor() {
	let color = window.localStorage.getItem('snakeColor');
	if(!color || window.localStorage.getItem('snakeColorName') !== getName()) {
		color = "#"+((1<<24)*Math.random()|0).toString(16);
		window.localStorage.setItem('snakeColor', color);
		window.localStorage.setItem('snakeColorName', getName());
	}
	return color;
}

function setGameId(gameId) {
	$("#gameId").html(gameId);
}
function setBotVersion(botVersion) {
	$('#botVersion').html(botVersion);
}
function setIteration(iteration) {
	$("#runVersion").html(iteration);
}
function setNMoves() {
	$("#nMoves").html(nMoves);
}

function refresh() {
	$('.current').html(current);
	paintSnapshot();
}

function paintSnapshot(snapshot) {
	const {board} = snapshot;
	const drawing = board.map(row => `<div class='row'>${row.map(cell => `<div class='${CELL_CLASS[cell]}'></div>`).join('')}</div>`).join('');
	$('#game').html(drawing);
}

function buildChart(divId, chart) {

	Highcharts.chart(divId, {

		title: {
			text: '',
		},

		yAxis: {
			title: {
				text: chart.name,
			}
		},

		xAxis: {
			title: {
				text: 'Game number',
			}
		},

	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },

	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	            pointStart: 0
	        }
	    },

	    series: [{
	    	...chart,
	    }],

	});

}

function hideCharts() {
	$(".chartsPopup").hide();
	cleanCharts();
}

function cleanCharts() {
	$(".tab").off();
	$(".tabs").html("");
	$(".body").html("");
	$currentChart = null;
}

function addTab(id, name) {
	$('.tabs').append(`<div class='tab' id='tab-${id}'>${name}</div>`);
}

function addChart(id, chart) {
	const highchartId = `highchart-${id}`;
	$('.body').append(`<div class='chart' id='chart-${id}'><div id='${highchartId}'></div></div>`);
	buildChart(highchartId, chart);
}

function updateUnreadMessages() {
	if(unreadMessages > 0) {
		$("#chatUnreadMessages").html(unreadMessages > 9 ? '+9' : unreadMessages);
		$("#chatUnreadMessages").css('visibility', 'visible');
	}
	else {
		$("#chatUnreadMessages").css('visibility', 'hidden');
	}
}

function showCharts(charts) {
	cleanCharts();
	$(".chartsPopup").show();
	Object.entries(charts).forEach(([id, chart]) => {
		const { name } = chart;
		addTab(id, name);
		addChart(id, chart);
	});
	$('.tab').on('click', function() {
		$('.tab').removeClass('selected');
		const id = $(this).attr('id').replace('tab-', '');
		$(`#tab-${id}`).addClass('selected');
		if($currentChart) $currentChart.hide();
		$currentChart = $(`#chart-${id}`);
		$currentChart.show();
	});
	$('.tabs').children().first().trigger('click');
}

function paintChat(chat) {
	chat.forEach(message => {
		chatAddMessage(message);
	});	
}