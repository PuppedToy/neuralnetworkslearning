let current = 1;

const keys = {
	'37': previous,
	'39': next,
	'78': nextGame,
	'80': previousGame,
};

const CELL_CLASS = {
	' ': 'cell',
	'O': 'cell snake',
	'H': 'cell snake head',
	'X': 'cell food',
};

$(document).ready(() => {

	$(".total").html(game.length);

	$(document).on('keydown', ({which}) => {
		console.log(which);
		if(which in keys) {
			keys[which]();
		}
	})

	$(".next").on("click", () => {
		next();
	});

	$(".previous").on("click", () => {
		previous();
	});

	paintSnapshot();

});

function next() {
	if(current < game.length) current++;
	refresh();
}

function previous() {
	if(current >= 2) current--;
	refresh();
}

function updateNumber(n, zeros, update) {
	const newN = String(parseInt(n)+update);
	const diff = newN.length-n.length;
	console.log(n, newN, diff);
	if(!diff) return `${zeros}${newN}`;
	else if(diff >= 1) return `${zeros.substr(1)}${newN}`;
	else return `0${zeros}${newN}`; 
}

function changeGame(update) {
	const urlRegex = /(.*?\/experiments\/[0-9]*?\/)(0*)([0-9]*?)(\/replay\.html.*)/;
	const urlMatch = urlRegex.exec(window.location.href);
	const [ _, prefix, zeros, runId, suffix ] = urlMatch;

	const newHREF = `${prefix}${updateNumber(runId, zeros, update)}${suffix}`;
	window.location.href = newHREF;
}

function nextGame() {
	changeGame(1);
}

function previousGame() {
	changeGame(-1);
}

function refresh() {
	$('.current').html(current);
	paintSnapshot();
}

function paintSnapshot() {
	const snapshot = game[current-1].board;
	const drawing = snapshot.map(row => `<div class='row'>${row.map(cell => `<div class='${CELL_CLASS[cell]}'></div>`).join('')}</div>`).join('');
	$('#game').html(drawing);
}