var { getLastCommit } = require('git-last-commit');
const fs = require('fs-extra');
fs.getLastCommit = getLastCommit;

function promsieFromFunction(f) {
	return function(...args) {
		return new Promise((resolve, reject) => {
			fs[f](...args, (err, result) => {
				if(err) return reject(err);
				resolve(result);
			});
		});
	};
}

const methods = ['readFile', 'writeFile', 'readdir', 'mkdir', 'access', 'getLastCommit'];
const variables = ['F_OK'];

module.exports = {
	...methods.reduce((object, method) => {
		return {
			...object,
			[method]: promsieFromFunction(method),
		};
	}, {}),
	...variables.reduce((object, variable) => {
		return {
			...object,
			[variable]: variable,
		};
	}, {}),
};
