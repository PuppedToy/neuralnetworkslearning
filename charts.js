class Charts {

	constructor() {
		this.moves = [];
		this.size = [];
		this.lengthAverageEvolution = [];
		this.nMovesAverageEvolution = [];
		this.exploreRates = [];
		this.nExploring = 0;
	}

	addGame(game, lastMove) {
		this.moves.push(lastMove.turn);
		this.size.push(game.getSnakeSize());
		this.lengthAverageEvolution.push(this.getCurrentAverage());
		this.nMovesAverageEvolution.push(this.getCurrentAverage(undefined, 'moves'));
		this.exploreRates.push(this.nExploring/lastMove.turn*100);
		this.nExploring = 0;
	}

	addMove(move) {
		this.nExploring += move.isExploring ? 1 : 0;
	}

	getCurrentAverage(trunk, chart = 'size') {
		let array = this[chart];
		if(trunk) array = array.slice(Math.max(0, array.length-trunk));
		return array.reduce((total, element) => total + element, 0)/array.length;
	}

	getCharts() {
		return {
			lengthAverageEvolution: {
				name: 'Snake length average evolution',
				data: this.lengthAverageEvolution,
			},
			nMovesAverageEvolution: {
				name: 'Snake moves per game average evolution',
				data: this.nMovesAverageEvolution,
			},
			exploreRates: {
				name: 'Exploration rate per game',
				data: this.exploreRates,
			},
			moves: {
				name: 'Snake moves',
				data: this.moves,
			},
		};
	}

}

module.exports = Charts;