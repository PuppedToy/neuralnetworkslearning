function sample(array) {
	return array[Math.min(parseInt(Math.random()*array.length), array.length-1)];
}

module.exports = {
	sample,
};
