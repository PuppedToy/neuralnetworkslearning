const Game = require('./snake');
const keypress = require('keypress');
const fs = require('fs-extra');

keypress(process.stdin);

const game = new Game();
process.stdout.write(game.snapshot().toString());

const keys = ['RIGHT', 'LEFT', 'UP', 'DOWN'];
process.stdin.on('keypress', function (ch, key) {
	let keyName = key.name.toUpperCase();
	if(keys.includes(keyName)) {
		if(!game.moveSnake(keyName).gameStillRunning) {
			const history = `const game = JSON.parse('${JSON.stringify(game.history)}');`;
			fs.writeFileSync('assets/lastgame.js', history);
			process.exit();
		}
		clear();
		process.stdout.write(game.snapshot().toString());
	}
	if (key && key.ctrl && key.name == 'c') {
		process.stdin.pause();
	}
});

process.stdin.setRawMode(true);
process.stdin.resume();

function clear() {
	const readline = require('readline')
	const blank = '\n'.repeat(process.stdout.rows)
	console.log(blank)
	readline.cursorTo(process.stdout, 0, 0)
	readline.clearScreenDown(process.stdout)
}